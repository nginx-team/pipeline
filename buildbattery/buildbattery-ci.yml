variables:
  APT_DIR: "${CI_PROJECT_DIR}/apt"
  RELEASE: 'unstable'
  SALSA_CI_IMAGES: 'registry.salsa.debian.org/salsa-ci-team/pipeline'
  SALSA_CI_IMAGES_BASE: "${SALSA_CI_IMAGES}/base:${RELEASE}"

.artifacts: &artifacts
  name: "$CI_JOB_NAME}"
  paths:
    - "${APT_DIR}/"
  expire_in: 1 day

.apt-local-apt: &add-local-apt
  - |
    (
      cd "${APT_DIR}/";
      eatmydata dpkg-scanpackages . /dev/null > Packages
      eatmydata sed -i 's,Filename: ./,Filename: ,' Packages
      eatmydata gzip -9 < Packages > Packages.gz
      (
        echo "Date: `date -Ru`"
        echo 'SHA256:'
        printf ' %s %16d %s\n' "`sha256sum Packages | awk '{print $1}'`" "`wc -c Packages | awk '{print $1}'`" 'Packages'
        printf ' %s %16d %s\n' "`sha256sum Packages.gz | awk '{print $1}'`" "`wc -c Packages.gz | awk '{print $1}'`" 'Packages.gz'
      ) > Release
    )
  - echo "deb [trusted=yes] file://${APT_DIR} ./" > /etc/apt/sources.list.d/local.list
  - eatmydata apt-get update

.build: &build
  stage: build
  image: "${SALSA_CI_IMAGES_BASE}"
  artifacts:
    <<: *artifacts
  script:
    # create artifacts directory
    - |
      if [ ! -d "${APT_DIR}/" ]; then
        mkdir -p "${APT_DIR}/"
      fi
    # install build packages
    - eatmydata apt-get update
    - eatmydata apt-get -y upgrade
    - eatmydata apt-get install --no-install-recommends -y build-essential git-buildpackage debhelper devscripts git equivs pristine-tar rsync dpkg-dev
    - *add-local-apt
    - GITDIR="$CI_PROJECT_DIR/${CI_JOB_NAME:6}"
    - eatmydata rm -rf "${GITDIR}"
    - eatmydata git clone "${URL}" "${GITDIR}"
    - cd "${GITDIR}"
    - eatmydata gbp pull --ignore-branch --pristine-tar --track-missing
    - |
      if [ ! -z "${BRANCH}" ]; then
        git checkout "${BRANCH}"
      fi
    - |
      if [ ! -z "${COMMIT}" ]; then
        git reset --hard "${COMMIT}"
      fi
    # install dependencies
    - |
      TMP_DIR="$CI_PROJECT_DIR/tmp/"
      rm -rf "${TMP_DIR}/"
      mkdir -p "${TMP_DIR}/.debdeps/"
      eatmydata rsync -a "${GITDIR}/" "${TMP_DIR}/.debdeps/"
      (
        cd "${TMP_DIR}/.debdeps/"
        eatmydata mk-build-deps -i -r -t "apt-get -f -qqy --force-yes" || {
          # XXX mk-build-deps fails silently, try to get error message from dpkg
          echo "mk-build-deps FAILED: "
          dpkg -i "${TMP_DIR}/"*.deb
        }
      )
      rm -rf "${TMP_DIR}/"
    # TODO check here, "`dpkg -l | grep '+salsaci+'`"
    # update changelog
    - sed -i -e "1 s/)/+salsaci+`date +'%Y%m%d'`+${CI_PIPELINE_IID})/" debian/changelog
    # build
    - eatmydata dpkg-buildpackage -b
    # copy artifacts
    - eatmydata cp ../*.deb ../*.changes ../*.buildinfo "${APT_DIR}/"
    - rm "${APT_DIR}/Packages.gz" "${APT_DIR}/Packages" "${APT_DIR}/Release"

.install: &install
  stage: install
  image: "${SALSA_CI_IMAGES_BASE}"
  artifacts:
    <<: *artifacts
  script:
    - eatmydata apt-get update
    - eatmydata apt-get install --no-install-recommends -y dpkg-dev curl
    - *add-local-apt
    # install nginx and modules
    - PACKAGES=`cd "${APT_DIR}/"; ls -1 *.deb | grep -v 'dbg' | cut -d'_' -f1`
    - echo ${PACKAGES}
    - eatmydata apt-get install --no-install-recommends -y nginx ${PACKAGES}
    # test curl
    - /etc/init.d/nginx start
    - |
      curl --silent --fail -o /dev/null -w "response_code: %{http_code}\n" http://127.0.0.1/
    # print installed packages
    - dpkg -l | grep nginx
    - test -n "`dpkg -l | grep '+salsaci+'`"
    # print apt
    - echo "\"deb [trusted=yes] ${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}/artifacts/raw/apt /\""

.upgrade: &upgrade
  stage: upgrade
  image: $SALSA_CI_IMAGES_BASE
  needs:
    - job: "install"
      artifacts: true
  script:
    - ls -l /etc/apt/sources.list.d/
    - "cat /etc/apt/sources.list || :"
    - "cat /etc/apt/sources.list.d/* || :"
    - 'echo "deb http://deb.debian.org/debian stable main" > /etc/apt/sources.list.d/stable.list'
    - eatmydata apt-get update
    - eatmydata apt-get -y update
    # install from stable
    - '[ ! -z "${PACKAGES}" ] || PACKAGES="${CI_JOB_NAME:8}"'
    - eatmydata apt-get install -t stable --no-install-recommends -y nginx ${PACKAGES}
    - 'ls -lah /etc/nginx/'
    - 'dpkg -l | grep "nginx\|lua"'
    # upgrade
    - 'echo "deb [trusted=yes] file://${APT_DIR} ./" > /etc/apt/sources.list.d/local.list'
    - eatmydata apt-get update
    - eatmydata apt-get -y install aptitude
    - eatmydata apt-get -y dist-upgrade
    - 'ls -lah /etc/nginx/'
    - 'dpkg -l | grep "nginx\|lua"'
    - aptitude -y purge ?config-files
    - 'ls -lah /etc/nginx/'
    - 'dpkg -l | grep "nginx\|lua"'

stages:
  - build
  - install
  - upgrade
