import sys

packages = {
    'nginx': { },
    'libnginx-mod-http-ndk': { 'deps': ['nginx',], },
    'libnginx-mod-http-auth-pam': { 'deps': ['nginx',] },
    'libnginx-mod-http-brotli': { 'deps': ['nginx',] },
    'libnginx-mod-http-cache-purge': { 'deps': ['nginx',] },
    'libnginx-mod-http-dav-ext': { 'deps': ['nginx',] },
    'libnginx-mod-http-echo': { 'deps': ['nginx',] },
    'libnginx-mod-http-fancyindex': { 'deps': ['nginx',] },
    'libnginx-mod-http-geoip2': { 'deps': ['nginx',] },
    'libnginx-mod-http-headers-more-filter': { 'deps': ['nginx',] },
    'lua-resty-core': { 'url': 'https://salsa.debian.org/lua-team/lua-resty-core.git', 'deps': ['lua-resty-lrucache',] },
    'lua-resty-lrucache': { 'url': 'https://salsa.debian.org/lua-team/lua-resty-lrucache.git' },
    'libnginx-mod-http-lua': { 'deps': ['lua-resty-core', 'libnginx-mod-http-ndk'] },
    'libnginx-mod-http-memc': { 'deps': ['nginx',] },
    'libnginx-mod-http-set-misc': { 'deps': ['libnginx-mod-http-ndk',] },
    'libnginx-mod-http-srcache-filter': { 'deps': ['libnginx-mod-http-ndk',] },
    'libnginx-mod-http-subs-filter': { 'deps': ['nginx',] },
    'libnginx-mod-http-uploadprogress': { 'deps': ['nginx',] },
    'libnginx-mod-http-upstream-fair': { 'deps': ['nginx',] },
    'libnginx-mod-js': { 'deps': ['nginx',] },
    'libnginx-mod-nchan': { 'deps': ['nginx',] },
    'libnginx-mod-rtmp': { 'deps': ['nginx',] },
}

# variables
print(f'#variables: ')
for package in packages:
    pkg = package
    pkgup = pkg.upper().replace('-', '_')
    if 'url' in packages[package]:
        url = packages[package]["url"]
    else:
        url = f"https://salsa.debian.org/nginx-team/{package}.git"
    #print(f'  {pkgup}_ENABLE: "false"')
    print(f'  #{pkgup}_URL: "{url}"')
#print(f'  LIBNGINX_MOD_ALL_ENABLE: "false"')
print('')

# build
for package in packages:
    if 'enable' in packages[package]:
        pkg = packages[package]['enable']
    else:
        pkg = package
    pkgup = pkg.upper().replace('-', '_')
    env_url = f'{pkgup}_URL'
    env_branch = f'{pkgup}_BRANCH'
    env_commit = f'{pkgup}_COMMIT'
    print(f'build {package}:')
    print(f'  extends: .build')
    print(f'  rules:')
    #print(f'    - if: $LIBNGINX_MOD_ALL_ENABLE =~ /^(1|yes|true)$/ || ${env_enable} =~ /^(1|yes|true)$/')
    print(f'    - if: ${env_url}')
    if 'deps' in packages[package]:
        deps=packages[package]['deps']
        print('  needs:')
        for dep in deps:
            print(f"    - job: 'build {dep}'")
            print(f'      artifacts: true')
            print(f'      optional: true')
    else:
        print('  dependencies: []')
    print(f'  variables:')
    print(f'    URL: "${env_url}"')
    print(f'    BRANCH: "${env_branch}"')
    print(f'    COMMIT: "${env_commit}"')
    print(f'')


# install
print('install:')
print('  extends: .install')
print('  needs:')
for package in packages:
    print(f"    - job: 'build {package}'")
    print('      artifacts: true')
    print('      optional: true')
print('')

# upgrade
print('upgrade nginx-common:')
print('  extends: .upgrade')
print('  variables:')
print('    PACKAGES: nginx-common')
print('  needs:')
print(f"    - job: 'install'")
print('      artifacts: true')
print('      optional: true')
print('')
