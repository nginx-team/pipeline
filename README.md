# Pipelines for Nginx-team

## Build battery

Build battery helps primarily for testing compilation of NGINX and its third-party modules.

### Simple Build 
As a simple example, compiling the libnginx-mod-http-echo module which e.g. depends
on NGINX changes that are in the experimental branch.

* nginx - from git `https://salsa.debian.org/nginx-team/nginx.git`, branch `experimental`
* libnginx-mod-http-echo - from git `https://salsa.debian.org/nginx-team/libnginx-mod-http-echo.git`

```yaml
---
include:
 - https://salsa.debian.org/nginx-team/pipeline/raw/main/buildbattery/buildbattery.yml

variables:
  NGINX_URL: 'https://salsa.debian.org/nginx-team/nginx.git'
  NGINX_BRANCH: 'experimental'
  LIBNGINX_MOD_HTTP_ECHO_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-echo.git'
```

### Build dependencies
Some modules are dependent on other modules.
Special example is libnginx-mod-http-lua which depends on libnginx-mod-http-ndk, lua-resty-core.
And lua-resty-core and libnginx-mod-http-lua depend on each other and has IMPLICIT dependency.
The build battery solves the dependency problem.
Here is example how to test it:

* nginx - from `unstable`
* libnginx-mod-http-ndk - from `unstable`
* lua-resty-core - from git `https://salsa.debian.org/lua-team/lua-resty-core.git`
* libnginx-mod-http-lua - from git `https://salsa.debian.org/nginx-team/libnginx-mod-http-lua.git`

```yaml
---
include:
 - https://salsa.debian.org/nginx-team/pipeline/raw/main/buildbattery/buildbattery.yml

variables:
  LUA_RESTY_CORE_URL: 'https://salsa.debian.org/lua-team/lua-resty-core.git'
  LIBNGINX_MOD_HTTP_LUA_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-lua.git'
```

### Build all
Since nginx has third-party modules, it is necessary to maintain ABI compatibility.
If structures/dependencies/... change in nginx, you have to change the ABI version and recompile all modules.
Here's an example to test it:

```yaml
---
include:
 - https://salsa.debian.org/nginx-team/pipeline/raw/main/buildbattery/buildbattery.yml

variables:
  NGINX_URL: 'https://salsa.debian.org/nginx-team/nginx.git'
  LIBNGINX_MOD_HTTP_NDK_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-ndk.git'
  LIBNGINX_MOD_HTTP_AUTH_PAM_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-auth-pam.git'
  LIBNGINX_MOD_HTTP_BROTLI_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-brotli.git'
  LIBNGINX_MOD_HTTP_CACHE_PURGE_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-cache-purge.git'
  LIBNGINX_MOD_HTTP_DAV_EXT_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-dav-ext.git'
  LIBNGINX_MOD_HTTP_ECHO_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-echo.git'
  LIBNGINX_MOD_HTTP_FANCYINDEX_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-fancyindex.git'
  LIBNGINX_MOD_HTTP_GEOIP2_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-geoip2.git'
  LIBNGINX_MOD_HTTP_HEADERS_MORE_FILTER_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-headers-more-filter.git'
  LUA_RESTY_CORE_URL: 'https://salsa.debian.org/lua-team/lua-resty-core.git'
  LIBNGINX_MOD_HTTP_LUA_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-lua.git'
  LIBNGINX_MOD_HTTP_MEMC_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-memc.git'
  LIBNGINX_MOD_HTTP_SET_MISC_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-set-misc.git'
  LIBNGINX_MOD_HTTP_SRCACHE_FILTER_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-srcache-filter.git'
  LIBNGINX_MOD_HTTP_SUBS_FILTER_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-subs-filter.git'
  LIBNGINX_MOD_HTTP_UPLOADPROGRESS_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-uploadprogress.git'
  LIBNGINX_MOD_HTTP_UPSTREAM_FAIR_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-http-upstream-fair.git'
  LIBNGINX_MOD_JS_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-js.git'
  LIBNGINX_MOD_NCHAN_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-nchan.git'
  LIBNGINX_MOD_RTMP_URL: 'https://salsa.debian.org/nginx-team/libnginx-mod-rtmp.git'
```
